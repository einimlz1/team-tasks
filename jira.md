# JIRA Instances

| Version              | URL                               | Login                              | Admin          | SSH Access |
|----------------------|-----------------------------------|------------------------------------|----------------| ---------- |
| Jira Server 8.5.5    | http://jira.reali.sh:8080/        | Need an account from admin (see credentials in 1Password Engineering vault) | Patrick Deuley | Yes, via GCP Console |
| Jira Cloud           | https://olearycrew.atlassian.net/ | Sign in with Google account        | Brendan O'Leary| N/A        |
| Jira Cloud           | https://gitlabknowledge.atlassian.net/ | Need an account from admin    | Christen Dybenko | N/A      |
| Jira Cloud           | https://gitlab-jira.atlassian.net/ | Need an account from admin    | Tye Davis & Gabe Weaver | N/A      |
| Jira Cloud           | Your own custom instance          | Register an Atlassian account and set up a free instance | You! | N/A |

## Version Notes
It is important to test features using self-hosted Jira (Jira Core and Software) as well as Jira Cloud as the APIs differ between the two.

Also important to understand the different types of Jira self-hosted offerings. [Differences between Jira Core, Service Desk, Software](http://www.akeles.com/what-are-the-differences-between-jira-software-jira-service-desk-and-jira-core/)

## SSH Access
Instances where `SSH Access` is listed as `Yes` are instances where Ecosystem team members have root SSH access and are able to administer the instance. The listed Admin is still the only person who can administer the instance VM from GCP (Google Cloud Platform).

## Notes for Installing Jira on a Linux Server
* https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-938846823.html
* https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-from-archive-file-938846844.html
* The Jira home directory is located at `/opt/atlassian/jira`
* Logs located at `/opt/atlassian/jira/logs`
* [Setting up SSL](https://confluence.atlassian.com/adminjiraserver/running-jira-applications-over-ssl-or-https-938847764.html#RunningJiraapplicationsoverSSLorHTTPS-commandline)
  * Use `locate keytool` to find the path to Keytool with the Jira JRE
  * For newer Jira, when you add the `<Connector port="8443">` in `/opt/atlassian/jira/conf/server.xml` make sure it has properties `relaxedPathChars="[]|" relaxedQueryChars="[]|{}^&#x5c;&#x60;&quot;&lt;&gt;" `. [More info](https://confluence.atlassian.com/jirakb/jira-server-startup-fails-with-tomcat-misconfigured-error-958453799.html)
  * For a signed cert, [install certbot on server](https://certbot.eff.org/instructions)
    * Generate a CSR with JRE keytool `<JIRA_HOME>/jre/bin/keytool -certreq -alias jira -keystore <JIRA_HOME>/jira.jks` and save to file
    * Generate cert with Certbot `certbot certonly --csr <JIRA_HOME>/csr_file_name.csr`
    * Import cert with keytool `<JIRA_HOME>/jre/bin/keytool -import -alias jira -keystore <JIRA_HOME>/jira.jks -file 0001_chain.pem`
* Start Jira Server: `sudo /etc/init.d/jira start`
* Stop Jira Server: `sudo /etc/init.d/jira stop`
* Monitor Start Up Logs: `sudo tail -f /opt/atlassian/jira/logs/catalina.out`
