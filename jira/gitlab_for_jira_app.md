# GitLab for Jira App

We maintain the [GitLab for Jira App](https://marketplace.atlassian.com/apps/1221011/gitlab-for-jira?hosting=cloud&tab=overview) in the Atlassin marketplace.

## App Marketplace Settings

These are the current App settings. If you want to make changes, please submit an MR to change this file and ping Patrick Deuley or Andy Soiron to update it on the marketplace.

### Details

**Name**:
```
GitLab for Jira
```

**Tagline:** (limited to 130 characters)
```
Integrate commits, branches and merge requests from GitLab into Jira
```


**Summary:** (limited to 250 characters)

```
Integrates with Gitlab.com to display commits, branches and merge requests that reference a Jira Issue into the Development Panel.
```

### Version 1.0.0-AC

**Release summary:** (limited to 80 characters)

```
Initial Version
```

**More details:** (limited to 1000 characters)

```
This app integrates GitLab commits, branches and merge requests with Jira issues. The App can connect to GitLab.com but not to self managed GitLab instances.

When a Jira issue is mentioned in GitLab, the commit, branch, or merge request is shown on the issue's development panel.

Available for GitLab.com Silver and above.

[Learn more](https://docs.gitlab.com/ee/integration/jira_development_panel.html#gitlabcom-1) about how to set up Jira for Gitlab or watch [Configure GitLab Jira Integration using Marketplace App on YouTube](https://www.youtube.com/watch?v=SwR-g1s1zTo&feature=youtu.be) for a walkthrough.
```

**Release notes:** (limited to 1000 characters)

```
This is an initial version that allows linking of GitLab namespaces into Jira
```