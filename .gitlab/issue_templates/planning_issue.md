# Integrations: Top Goals

_narrative -- what are we doing?_

### 1. priority foo

**Why?** description bar

### 2. priority fizz

**Why?** description buzz

## Action Required

Please review your current stories and goals, to make sure they are aligned with the Milestone goals. Everyone should go through the stories that are really needed to help hitting these goals reliably towards the end of the milestone.

**What should I do?** Link Related Stories to the priority discussion blocks below. This might need some investigation to make sure we are not overlooking critical work, and that is acceptable. We can save a lot of time by spending time on preparation.

### 00.0 UX

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

### 00.0 Backend

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

### 00.0 Frontend

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

---

# Foundations: Top Goals

### UX OKR

* OKR foo

### Foundations OKR

* OKR foo

### 00.0 UX

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

### 00.0 Frontend

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

---

# GDK: Top Goals

Issues:

1. [ ] [Issue](url)
1. [ ] [Issue](url)
1. [ ] [Issue](url)

/assign @mushakov @arturoherrero @tauriedavis @leipert
